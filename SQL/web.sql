﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;
CREATE TABLE user (id SERIAL primary key,
                    login_id varchar(255),
                    name varchar(255),
                    birth_date DATE,
                    password varchar(255),
                    create_date DATETIME,
                    update_date DATETIME);
INSERT INTO user VALUES (1,'admin','管理者','1998-11-20','likeit',NOW(),NOW());
SELECT * FROM user;
SELECT * FROM user WHERE login_id ='admin' AND password ='n';
