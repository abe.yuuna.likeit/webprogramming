<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ユーザー一覧画面</title>
        <link rel="stylesheet" href="css/style.css" >
    </head>
    <body>
     <div class="clearfix">
            <div id="p1"><a href="LogoutServlet" >ログアウト</a></div>
            <div id="p2">${userInfo.name} さん</div>
    </div>


    <h1 align ="center">ユーザ一覧</h1>

        <div align ="right">
        <a href="RegisterServlet"> 新規登録</a>
        </div>

    <div align = "center">
    <form action="UserListServlet" method="post">
        <table class="u_list">

        <tr>
            <th>ログインID</th>
                <td>
                 <input type="text" name="loginId" value="" size="38">
                </td>
        </tr>

        <tr>
            <th>ユーザ名</th>
                <td>
                 <input type="text" name="user_name" value="" size="38">
                </td>
        </tr>

        <tr>
            <th>生年月日</th>
                <td>
                 <input type="date" name="brith_day" value="" size="24">
                <a>~</a>
                 <input type="date" name="brith_day2" value="" size="24">
                </td>
        </tr>

        </table>
        <div id="search"><button class="btn2" type="submit">検索</button></div>
		</form>
        <hr>

        <table border="1" >
    <tr id="listcolor">
        <th>ログインID</th>
        <th>ユーザ名</th>
        <th>生年月日</th>
        <th></th>
    </tr>
 	<c:forEach var="user" items="${userList}" >
    <tr>
        <td>${user.loginId}</td>
        <td>${user.name}</td>
        <td>${user.birthDate}</td>
        <td>
            <div class="btniti">
            <c:if test="${userInfo.loginId=='admin'}">
                <button class="btnlist" onclick="location.href='UserDetailServlet?id=${user.id}'">詳細</button>

                <button class="btnlist" onclick="location.href='UserUpdateServlet?id=${user.id}'">更新</button>

                <button class="btnlist" onclick="location.href='UserDeleteServlet?id=${user.id}'">削除</button>
            </c:if>
            <c:if test="${userInfo.loginId!='admin'}">
            <button class="btnlist" onclick="location.href='UserDetailServlet?id=${user.id}'">詳細</button>
            <c:if test="${userInfo.loginId==user.loginId}">
            <button class="btnlist" onclick="location.href='UserUpdateServlet?id=${user.id}'">更新</button>
            </c:if>
            </c:if>
            </div>
        </td>
    </tr>
    </c:forEach>
  </table>

    </div>
    </body>
</html>