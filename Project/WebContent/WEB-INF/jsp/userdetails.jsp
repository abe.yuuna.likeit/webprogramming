<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ユーザー詳細</title>
    <link rel="stylesheet" href="css/style.css" >
</head>

<body>
    <div class="clearfix">
            <div id="p1"><a href="LogoutServlet" >ログアウト</a></div>
            <div id="p2">${userInfo.name} さん</div>
    </div>

   <h1 align ="center">ユーザ情報詳細参照</h1>

    <div align ="center">
    <table class="u_list" >

    <tr>
        <th class="space">ログインID</th>
        <td>${user.loginId}</td>
    </tr>
    <tr>
        <th class="space">ユーザ名</th>
        <td>${user.name}</td>
    </tr>

    <tr>
        <th class="space">生年月日</th>
        <td>${user.birthDate}</td>
    </tr>

    <tr>
        <th class="space">登録日時</th>
        <td>${user.createDate}</td>
    </tr>

    <tr>
        <th class="space">更新日時</th>
        <td>${user.updateDate}</td>
    </tr>
  </table>

    </div>
    <a href="UserListServlet">戻る</a>
    </body>
</html>