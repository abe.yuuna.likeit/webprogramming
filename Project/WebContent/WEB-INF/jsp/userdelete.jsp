<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ユーザー情報削除</title>
    <link rel="stylesheet" href="css/style.css" >
</head>

<body>
    <div class="clearfix">
            <div id="p1"><a href="LogoutServlet" >ログアウト</a></div>
            <div id="p2">${userInfo.name} さん</div>
    </div>

   <h1 align ="center">ユーザ削除確認</h1>


    <div>${user.name}</div>
    <div>を本当に削除してよろしいでしょうか。</div>
    <div align ="center">
    	<form action="UserListServlet" method="get">
       <button class="btn" type="submit">キャンセル</button>
       </form>
       <form action="UserDeleteServlet" method="post">
       <input type="hidden" name="id" value="${user.id}">
       <button class="btn" type="submit">OK</button>
       </form>
    </div>
    </body>
</html>