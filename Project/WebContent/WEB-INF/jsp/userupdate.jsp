<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ユーザー情報更新</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="clearfix">
            <div id="p1"><a href="LogoutServlet" >ログアウト</a></div>
            <div id="p2">${userInfo.name} さん</div>
    </div>

   <h1 align ="center">ユーザ情報更新</h1>

   <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color:red;">
		  ${errMsg}
		</div>
	</c:if>

    <div align ="center">
    <form class="form-update" action="UserUpdateServlet" method="post">
    <input type="hidden" name="id" value="${user.id}">
    <table class="u_list">
    <tr>
        <th class="space">ログインID</th>
        <td>${user.loginId}</td>
    </tr>
    <tr>
        <th class="space">パスワード</th>
        <td><input type="password" name="password" value="" size="15"></td>
    </tr>

    <tr>
        <th class="space">パスワード（確認）</th>
        <td><input type="password" name="password2" value="" size="15"></td>
    </tr>

    <tr>
        <th class="space">ユーザ名</th>
        <td><input type="text" name="name" value="${user.name}" size="15"></td>
    </tr>

    <tr>
        <th class="space">生年月日</th>
        <td><input type="date" name="birth_day" value="${user.birthDate}" size="20"></td>
    </tr>
  </table>
  <button class="btn btn-lg btn-primary btn-block btn-signin">更新</button>
  </form>
    </div>
    <a href="UserListServlet">戻る</a>
    </body>
</html>