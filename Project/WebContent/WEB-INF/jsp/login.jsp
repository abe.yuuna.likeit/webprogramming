<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>ログイン</title>
    <link rel="stylesheet" href="css/style.css" >
</head>

<body>
	<div class="clearfix">
            <div id="p1">ユーザ管理システム</div>
            <div id="p2"></div>
    </div>
   <h1 align ="center">ログイン画面</h1>
   	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color:red;">
		  ${errMsg}
		</div>
	</c:if>

    <div align = "center">
    <form class="form-signin" action="LoginServlet" method="post">
    <table class="u_list">
        <tr>
            <th>ログインID</th>
                <td>
                <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
                </td>
        </tr>

        <tr>
            <th>パスワード</th>
                <td>
                 <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
                </td>
        </tr>

    </table>
        <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
    </form>
    </div>
    </body>
</html>