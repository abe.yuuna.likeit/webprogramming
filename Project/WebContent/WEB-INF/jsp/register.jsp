
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>新規登録</title>
        <link rel="stylesheet" href="css/style.css" >
    </head>
    <body>
        <div class="clearfix">
            <div id="p1"><a href="LogoutServlet" >ログアウト</a></div>
            <div id="p2">${userInfo.name} さん</div>
    </div>

    <h1 align ="center">ユーザ新規登録</h1>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert" style="color:red;">
		  ${errMsg}
		</div>
	</c:if>

    <div align = "center">
    <form class="form-register" action="RegisterServlet" method="post">
        <table class="u_list">
        <tr>
            <th>ログインID</th>
                <td>
                 <input type="text" name="loginId" value="" size="24">
                </td>
        </tr>

        <tr>
            <th>パスワード</th>
                <td>
                 <input type="password" name="password" value="" size="24">
                </td>
        </tr>

        <tr>
            <th>パスワード（確認）</th>
                <td>
                 <input type="password" name="password2" value="" size="24">
                </td>
        </tr>

        <tr>
            <th>ユーザ名</th>
                <td>
                 <input type="text" name="name" value="" size="24">
                </td>
        </tr>

        <tr>
            <th>生年月日</th>
                <td>
                 <input type="date" name="birth_date" value="" size="24">
                </td>
        </tr>

    </table>

        <button class="btn btn-lg btn-primary btn-block btn-signin">登録</button>
	</form>

	<a href="UserListServlet">戻る</a>
        </div>



    </body>
</html>